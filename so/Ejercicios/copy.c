
#include <stdio.h>
int main(int argc, char *argv[]){
    FILE *file1, *file2;
    char ch;
    if(argc == 3){
        file1 = fopen(argv[1],"r");
        file2 = fopen(argv[2],"w");
        do{
            ch = fgetc(file1);
            fputc(ch,file2);
        }while(ch!=EOF);
        printf("\ncopiado %s a %s \n", argv[1], argv[2]);
        fclose(file1);
        fclose(file2);
    }
    else{
        printf("hay un numero no correcto de archivos\n");
    }
} 
