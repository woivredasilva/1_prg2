#include<stdio.h>
#include<stdlib.h>
int fork();
int getpid();
int getppid();
int wait();
int main(){
	for(int i=0;i<8;i++){
		if(fork() == 0){
		printf("Un hijo con pid %d con padre pid de %d\n",getpid(),getppid());
		exit(0);
		}
	}
	for(int i=0;i<8;i++)
	wait(NULL);
}
