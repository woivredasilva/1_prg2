#include<stdio.h>
#include<stdlib.h>

int fork();
int getpid();
int getppid();

int main(){
pid_t pid;
pid = fork();

switch(pid){
	case -1:
		printf("error\n");
		break;
	case 0:
		printf("Soy el hijo de PID %d\n",getpid());
		pid_t pit;
		pit = fork();
		switch(pit){
			case -1:
				printf("error\n");
				break;
			case 0:
				printf("Soy el nieto con PID %d , de el proceso %d\n",getpid(),getppid());
				break;
			default:
				printf("Soy el hijo de PID %d\n",getpid());
		}
		break;
	default:
		printf("Soy el padre de PID %d\n",getpid());
}
printf("Fin de ejecuccion\n");
exit(0);
}
