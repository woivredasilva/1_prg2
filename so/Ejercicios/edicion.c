#include<stdio.h>
#include<stdlib.h>

int fork();
int getpid();

int main(){
FILE *fd;
char hij = *"h";
char pad = *"p";
fd = fopen("file.txt", "w");
pid_t pid;
pid = fork();

switch(pid){
	case -1:
		printf("error\n");
		break;
	case 0:
		printf("Soy el hijo de PID %d\n",getpid());
		fputc(hij,fd);
		break;
	default:
		printf("Soy el padre de PID %d\n",getpid());
		fputc(pad,fd);
}
fclose(fd);
}

