#include <stdio.h>

void main(int argc, char *argv[]){    

int atoi();
int operador1;
int operador2;
int resultado;

if (argc<=1){
		printf("Error. Valores introducidos insuficientes");
	} else if (argc <3){
		printf("Error. Demasiados valores introducidos");
	} else {
	operador1 = atoi(argv[1]);
	operador2 = atoi(argv[2]);
	resultado = operador1 * operador2;
	printf("%d\n", resultado);
}
}
