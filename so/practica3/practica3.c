#include <stdio.h>
#include <semaphore.h>

sem_t sillas;
sem_t sofa;
sem_t local;
int peludo = 0;
int sleep();
int pthread_create();
int *trabajo = 0;
int wait();


void cortar(int i){ //La accion del peluquero
	switch(i){
		case 1: //peluquero activado
		sem_post(&sofa); //al llenar la silla alguien ocupa el sofa
		printf("Cortando un pelo \n"); 
		sleep(5); //espera los 5 segundos
		sem_post(&sillas); //señala que la silla esta libre
		sem_post(&local); //al irse de la silla puede entrar uno mas
		break;
		
		case 0: //peluquero desactivado
		sleep(NULL);
		break;
		}
}
void esperar(int i){ //La accion del cliente
	
	sem_wait(&local); //mira si el local esta lleno
	printf("Entra en Local %d \n",i);
	sem_wait(&sofa); //mira si hay un sitio en el sofa
	printf("Entra en Sofa %d \n",i);
	sem_wait(&sillas); //mira si es su turno
	printf("Entra en Cortar %d \n",i);
	cortar(1); //activa al peluquero
	
	
	
}

void main(){

pthread_t cliente; //thread del cliente
pthread_t peluquero; //thread del peluquero
sem_init(&local,0,20); //se inicializan los semaforos con local 20 para los 20 de aforo maximo
sem_init(&sofa,0,4); //4 por los espacios del sofa
sem_init(&sillas,0,3); //3 por los 3 peluqueros que hay 

int j = 0;
while(j < 3){ //crea los 3 thread de peluquero
pthread_create(&peluquero,NULL,cortar,0);
printf("Peluquero creado \n");
j++;
}
int i;
while(i<50){ //crea los 50 thread de cliente
if(i == 49){
sleep(35);
i++;
}
else{
pthread_create(&cliente,NULL,esperar,i);
sleep(1); //espera un segundo por cliente
i++;
}
}

}




