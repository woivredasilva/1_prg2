# Repositorio de Programación II y SO #

Aquí estarán los trabajos de Programación 2 (no siempre estarán actualizados) y los ejercicios de Sistemas Operativos.

* [IntraCEU](https://ceu.blackboard.com/ultra/courses/_137221_1/cl/outline)
* [Among Us](https://store.steampowered.com/app/945360/Among_Us/)

## Programación II ##

De momento, lo que llevamos es:

* Práctica sobre el **número π**: Compilada, terminada y corregida.

* Práctica sobre el **juego de la vida**: Compilada, pero no terminada (Solo plantilla).

Más sobre la carpeta `prg`:

* La carpeta se divide en ejercicios y prácticas.

* Algunos de los ejercicios no compilan: son para evaluar la información que Mariano ha ido dando en clase, que puede ser útil para realizar las prácticas.

## Sistemas Operativos ##

Sobre la carpeta `so`:

* Aquí estarán los ejercicios que ha ido mandando. 

+ En la carpeta `ejercicios_anteriores` estarán los ejercicios que ya se han mandado **sin el ejecutable**
+ En la carpeta `ejercicios_ahora` estarán los ejercicios que hay que entregar esa misma semana. Una vezestén resueltos, estos se deben de mover a la carpeta de `ejercicios_anteriores` para evitar complicaciones. El comando a utilizar es:

`mv * ejercicios_anteriores` (**desde la carpeta de `ejercicios_ahora`**)
