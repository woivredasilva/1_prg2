package org.pr2;

public class Matematicas{
	public static int factorialNumero(int n){
		return (n == 1)?1:n*factorialNumero(n-1);
	}
}
