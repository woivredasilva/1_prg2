public class Principal{
	public static double integral(double a, double b, double h){
			double suma = 0.0;
			for double (x = a; x <= b; x += h)
				suma += h * Math.exp(Math.pow(x, 2.0));
			return suma;	
		}
		
		//Function es una interfaz que pide un double y devuelve un double
		public static double integral(double a, double b, double h, Function<Double, Double> f){
			double suma = 0.0;
		for double(x = a; x <= b; x += h)
				suma += h * f.apply(x);
			return suma;
		}

		public static void main(String[] args){
			System.out.println("La integral de e^x^2 entre 0 y 1 es " + integral(0, 1, 0.001));
			System.out.println("La integral de e^x^2 entre 0 y 1 es " + integral(0, 1, 0.001, new ExponencialAlCuadrado);
		}
}
