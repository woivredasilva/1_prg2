import java.util.function.Function;

//Para hacerlo con otra Function, tendrías que crear otra clase con la nueva función
public class ExponencialDelCuadrado implements Function<Double, Double>{
	public Double apply(Double x){
		return Math.exp(Math.pow(x, 2.0));
	}
}
