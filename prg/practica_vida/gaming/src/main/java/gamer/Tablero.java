package gamer;


public class Tablero{

	private static int DIMENSION = 30;
	private int[][] estadoActual = new int[DIMENSION][DIMENSION];
	private int[][] estadoSiguiente = new int[DIMENSION][DIMENSION];


	
	public void  leerEstadoActual(){
//Todo par de "for" hacen la funcion de ir por cada posicion, es decir, i seria la x e j seria la y , e iria de modo tablero[0][0],tablero[0][1]... hasta tablero[0][DIMENSION] donde pasaria a tablero [1][0] y asi hasta tablero[DIMENSION][DIMENSION]
	for(int i = 0; i < DIMENSION ; i++){
		for(int j = 0; j < DIMENSION; j++){
			double generador = Math.random()*(1)+(-1);
			if(generador <= 0.5){ //Un simple 50/50 para poner los 1 y 0
				estadoActual[i][j] = 1;
			}	
			else estadoActual[i][j] = 0;
		}
		}
	} 

	
	public void generarEstadoActualPorMontecarlo(){
		//mismo par de for
		for(int i=0; i < DIMENSION; i++){
			for(int j = 0; j < DIMENSION; j++){
			int vecinos = 0;
			//vamos a saber los veinos usando la misma logica solo que en ved de ir de 0,0 a DIMENSION,DIMENSION vamos desde la coordenada ariba a la izquierda de la coordenada actual (es decir la [i-1][j-1]) hasta la esquina inferior derecha ([i+1][j+1]	
			for(int m=-1;m<=1;m++){
				for(int n=-1;n<=1;n++){
				vecinos = vecinos + estadoActual[i+m][j+n]; 
				}

			}	
			vecinos = vecinos - estadoActual[i][j]; //en el par de for anterior como se pasa por n=0 y m=0 se borra el estado actual para no añadir mas vecinos de la cuenta
			if(estadoActual[i][j] == 1){
			switch(vecinos){
			//Como solo se mantiene vivo si hay 2 o 3 vecinos simplemente añadimos este case y el resto son 0
			//el case 2 y 3 serian la regla de "Si tiene 2 o 3 vecinos sobrevive la generacion"
			//El resto son "Si tiene menos de 2 vecinos muere por falta de poblacion y si tiene mas de 4 vecinos es sobrepoblacion"
			case 2:
				estadoSiguiente[i][j] = 1;
				break;
			case 3:
				estadoSiguiente[i][j] = 1;
				break;
			default:
				estadoSiguiente[i][j] = 0;

				}

			}
			else{ //else porque solo puede ser o 1 o 0
			switch(vecinos){
				//Aqui se implementa de que si hay 3 vecinos exacto se crea uno nuevo en el 0
			case 3:
				estadoSiguiente[i][j] = 1;
				break;
			default:
				estadoSiguiente[i][j] = 0;
				}
			}
		}
		}	
	} 
	
	
	public void transitarAlEstadoSiguiente(){
	estadoActual = estadoSiguiente;
	estadoSiguiente = new int[DIMENSION][DIMENSION];
	System.out.println(estadoActual.toString());
	}


//La funcion del toString() es poder convertir estadoActual que es un grid a texto yendo coordenada a coordenada y añadiendo al string el valor de cada una pero añadiendo el salto de linea (\n) cada vez que se llegue al final de j	
	@Override
	public String toString(){
	String tableroActual = "";
	for(int i = 0; i < DIMENSION; i++){
		for(int j = 0; j < DIMENSION; j++){
			if(estadoActual[i][j] == 1) tableroActual += "1";
			else tableroActual += "0";
		}
		tableroActual += "\n";

	}	
	return tableroActual;
	}	
}
