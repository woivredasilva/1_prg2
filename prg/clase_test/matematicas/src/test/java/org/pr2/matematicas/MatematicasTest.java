package org.pr2.matematicas;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class MatematicasTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testSuma1()
    {
        assertEquals(1, Matematicas.suma(1) );
    }

    @Test
    public void testMayorQue1(){
    	assertEquals(15, Matematicas.suma(5));
    }

    //¡Ojo el = y el .class!    
    @Test(expected = ArithmeticException.class)
    public void testNumeroNegativo(){
    	Matematicas.suma(-1);
    }
}
