package org.pr2.matematicas;

public class Matematicas{
	public static int suma(int n){
		return (n == 1)?1:n+suma(n-1);
	}
}
