public class Principal{
	public static int suma(int n){
		if (n < 1) throw new ArothmeticException("El método 'int suma(int)' solo admite  
		return (n == 1)?1:n + suma(n - 1);
	}

	public static void main(String[] args){
		System.out.println("La suma de los 5 primeros positivos "  + suma(5));
		System.out.println(suma(-5));
	}
}
