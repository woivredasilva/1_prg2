
public class Matematicas{
	public static double generarNumeroPi (long pasos){
	double areaCuadrado = 4;
	int aciertos = 0;
	int radio = 1;

	for (int i = 1; i <= pasos; i++){
		double x = Math.random()*(1)+(-1); 
		double y = Math.random()*(1)+(-1); 
		if (Math.sqrt(x*x + y*y) <= 1){
			aciertos += 1;
		}
	}

	double areaCirculo = areaCuadrado*(aciertos/pasos);
	double estimacionPi = areaCirculo/(radio*radio);
	return estimacionPi;
	}
}


